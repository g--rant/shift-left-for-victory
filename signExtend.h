
#include<stdint.h>

#ifndef SIGNEXTEND_H
#define SIGNEXTEND_H


class signExtend{

private: 
	int8_t* pi_in; 
	int16_t* pi_out;


public:
	signExtend(int8_t* data_in, int16_t* data_out);
	~signExtend();
	void extend();

	

};

#endif