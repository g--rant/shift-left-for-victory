/**
*	File: EX_MEM_reg.h
*	Authors: Shift Left for Victory
*
*	Description: h file to implement a class that simulates the EXE/MEM inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*		WB Enable
*		WB Select
*		Mem Write Enable
*		Mem Read Enable
*		Mem doBranch
*		Rt Value
*		Rd
*		Alu Zero Signal
*		Alu Result
**/
#include<stdint.h>

#ifndef EX_MEM_REG_H
#define EX_MEM_REG_H

class EX_MEM_reg
{
private:
	bool* pb_WBEnableInput;
	bool* pb_WBSelectInput;
	bool* pb_MemWriteEnableInput;
	bool* pb_MemReadEnableInput;
	bool* pb_MemdoBranchInput;
	int16_t* pi_RtValueInput;
	int16_t* pi_RDInput;
	bool* pb_ALUZeroSignalInput;
	int16_t* pi_ALUResultInput;
	int16_t* pi_branchAddrInput;

	bool* pb_WBEnableOutput;
	bool* pb_WBSelectOutput;
	bool* pb_MemWriteEnableOutput;
	bool* pb_MemReadEnableOutput;
	bool* pb_MemdoBranchOutput;
	int16_t* pi_RtValueOutput;
	int16_t* pi_RDOutput;
	bool* pb_ALUZeroSignalOutput;
	int16_t* pi_ALUResultOutput;
	int16_t* pi_branchAddrOutput;


public:
	//Construct
	EX_MEM_reg(bool* pb_WBEnableIn, bool* pb_WBSelectIn, bool* pb_MemWriteEnableIn, bool* pb_MemReadEnableIn, bool* pb_MemdoBranchIn, int16_t* pi_RtValueIn, int16_t* pi_RDIn, bool* pb_ALUZeroSignalIn, int16_t* pi_ALUResultIn, int16_t* pi_branchAddrIn,
			bool* pb_WBEnableOut, bool* pb_WBSelectOut, bool* pb_MemWriteEnableOut, bool* pb_MemReadEnableOut, bool* pb_MemdoBranchOut, int16_t* pi_RtValueOut, int16_t* pi_RDOut, bool* pb_ALUZeroSignalOut, int16_t* pi_ALUResultOut, int16_t* pi_branchAddrOut);
	
	//Destructor
	~EX_MEM_reg();

	//Pass Over Function
	void EX_MEMFunct();


};

#endif