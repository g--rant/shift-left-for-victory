// FindOutput.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stdint.h>

int _tmain(int argc, _TCHAR* argv[])
{
	
	int counter = 0;
	int v0 = 0x0040;
	int v1 = 0x1010;
	int v2 = 0x000F;
	int v3 = 0x00F0;

	int t0 = 0x0000;
	int a0 = 0x0000;
	int a1 = 0x0005;

	int mem[] = { 0x0101, 0x0110, 0x0011, 0x00F0, 0x00FF };

	printf("Loop 0:\n");
	printf("r4: %X  r5: %X  r6: %X  r7: %X\n", v0, v1, v2, v3);
	printf("r8: %X  r2 %X, r3 %X\n", t0, a0, a1);
	printf("  mem[0]: %X\n  mem[1]: %X\n  mem[2]: %X\n  mem[3]: %X\n  mem[4]: %X\n", mem[0], mem[1],
		mem[2], mem[3], mem[4]);
	  

	while (a1 > 0){
		a1 = a1 - 1;
		t0 = mem[a0];

		if (t0 > 0x100){
			v0 = v0 / 8;
			v1 = v1 | v0;
			mem[a0] = 0xFF00;
		}
		else
		{
			v2 = v2 * 4; 
			v3 = v3 ^ v2;
			mem[a0] = 0x00FF;
		}
		a0 = a0 + 1;

		counter++;

		printf("Loop %i:\n", counter);
		printf("r4: %X  r5: %X  r6: %X  r7: %X\n", v0, v1, v2, v3);
		printf("r8: %X  r2 %X, r3 %X\n", t0, a0, a1);
		printf("mem[0]: %X\n  mem[1]: %X\n  mem[2]: %X\n  mem[3]: %X\n  mem[4]: %X\n", mem[0], mem[1],
			mem[2], mem[3], mem[4]);
	}
	
	system("pause");
	return 0;
}

