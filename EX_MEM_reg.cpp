/**
*	File: EX_MEM_reg.cpp
*	Authors: Shift Left for Victory
*
*	Description: cpp file to implement a class that simulates the EXE/MEM inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*		WB Enable
*		WB Select
*		Mem Write Enable
*		Mem Read Enable
*		Mem doBranch
*		Rt Value
*		Rd
*		Alu Zero Signal
*		Alu Result
**/

#include "EX_MEM_reg.h"

//Constructor
EX_MEM_reg::EX_MEM_reg(bool* pb_WBEnableIn, bool* pb_WBSelectIn, bool* pb_MemWriteEnableIn, bool* pb_MemReadEnableIn, bool* pb_MemdoBranchIn, int16_t* pi_RtValueIn, int16_t* pi_RDIn, bool* pb_ALUZeroSignalIn, int16_t* pi_ALUResultIn, int16_t* pi_branchAddrIn,
						bool* pb_WBEnableOut, bool* pb_WBSelectOut, bool* pb_MemWriteEnableOut, bool* pb_MemReadEnableOut, bool* pb_MemdoBranchOut, int16_t* pi_RtValueOut, int16_t* pi_RDOut, bool* pb_ALUZeroSignalOut, int16_t* pi_ALUResultOut, int16_t*pi_branchAddrOut){
	
	pb_WBEnableInput = pb_WBEnableIn;
	pb_WBSelectInput = pb_WBSelectIn;
	pb_MemWriteEnableInput = pb_MemWriteEnableIn;
	pb_MemReadEnableInput = pb_MemReadEnableIn;
	pb_MemdoBranchInput = pb_MemdoBranchIn;
	pi_RtValueInput = pi_RtValueIn;
	pi_RDInput = pi_RDIn;
	pb_ALUZeroSignalInput = pb_ALUZeroSignalIn;
	pi_ALUResultInput = pi_ALUResultIn;
	pi_branchAddrInput = pi_branchAddrIn;

	pb_WBEnableOutput = pb_WBEnableOut;
	pb_WBSelectOutput = pb_WBSelectOut;
	pb_MemWriteEnableOutput = pb_MemWriteEnableOut;
	pb_MemReadEnableOutput = pb_MemReadEnableOut;
	pb_MemdoBranchOutput = pb_MemdoBranchOut;
	pi_RtValueOutput = pi_RtValueOut;
	pi_RDOutput = pi_RDOut;
	pb_ALUZeroSignalOutput = pb_ALUZeroSignalOut;
	pi_ALUResultOutput = pi_ALUResultOut;
	pi_branchAddrOutput = pi_branchAddrOut;

}//end contsructor

//Destructor
EX_MEM_reg::~EX_MEM_reg(){
}//end Destructor

//Function that shifts internal stage one (output from the Decode Stage) to internal stage two (output to the Execute Stage)
void EX_MEM_reg::EX_MEMFunct(){
	
	*pb_WBEnableOutput = *pb_WBEnableInput;
	*pb_WBSelectOutput = *pb_WBSelectInput;
	*pb_MemWriteEnableOutput = *pb_MemWriteEnableInput;
	*pb_MemReadEnableOutput = *pb_MemReadEnableInput;
	*pb_MemdoBranchOutput = *pb_MemdoBranchInput;
	*pi_RtValueOutput = *pi_RtValueInput;
	*pi_RDOutput = *pi_RDInput;
	*pb_ALUZeroSignalOutput = *pb_ALUZeroSignalInput;
	*pi_ALUResultOutput = *pi_ALUResultInput;
	*pi_branchAddrOutput = *pi_branchAddrInput;
}//end EX_MEMFunct

