/**
*	File: control.cpp
*	Authors: Shift Left for Victory
*
*	Description: This file implements the control structure of our hardware.
*
**/

#include "control.h"

//Constructor
Control::Control(int16_t* ControlInputIn, bool* RegWriteIn, bool* RegSrcIn, 
	bool* RegDestIn, int16_t* ALUSrcIn, int16_t* ALUOpIn, bool* BranchIn, 
	bool* MemReadIn, bool* MemWriteIn, bool* MemtoRegIn, bool* BranchOrJumpIn, bool* isBranchIn, int16_t *isRsMuxControl){
	
	ControlInput = ControlInputIn;
	RegWrite = RegWriteIn;
	RegSrc = RegSrcIn;
	RegDest = RegDestIn;
	ALUSrc = ALUSrcIn;
	ALUOp = ALUOpIn;
	Branch = BranchIn;
	MemRead = MemReadIn;
	MemWrite = MemWriteIn;
	MemtoReg = MemtoRegIn;
	BranchOrJump = BranchOrJumpIn;
	isBranch = isBranchIn;
	RsMuxControl = isRsMuxControl;
}//end constructor

//Destructor
Control::~Control(){
}//end Destructor

void Control::UpdateControl(){
	uint16_t rawOp = (uint16_t)(*ControlInput);
	OpCode = rawOp>> 12;
	switch (OpCode)
	{
		
	case 0x0://0000 Nop
		*RegWrite = false;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 0;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;

	case 0x1:// 0001 Add
		*RegWrite = true;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 1;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;

	case 0x2://0010 Sub
		*RegWrite = true;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 2;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;

	case 0x3://0011 SLT
		*RegWrite = true;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 3;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;
	
	case 0x4://0100 OR
		*RegWrite = true;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 4;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;

	case 0x5://0101 Xor
		*RegWrite = true;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 5;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;
	case 0x6://0110 J
		*RegWrite = false;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 6;
		*BranchOrJump = true;
		*Branch = true;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = false;
		*isBranch = false;
		break;

	case 0x8://1000 Sll
		*RegWrite = true;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 1;
		*ALUOp = 8;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;
	
	case 0x9://1001 Srl
		*RegWrite = true;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 1;
		*ALUOp = 9;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;

	case 0xa://1010 Addi
		*RegWrite = true;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 1;
		*ALUOp = 10;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;
	
	case 0xb://1011 LW
		*RegWrite = true;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 1;
		*ALUOp = 11;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = true;
		*MemWrite = false;
		*MemtoReg = false;
		*isBranch = false;
		break;

	case 0xc://1100 SW
		*RegWrite = false;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 1;
		*ALUOp = 12;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = true;
		*MemtoReg = false;
		*isBranch = true;
		break;

	case 0xd://1101 Beq
		*RegWrite = false;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 0;
		*ALUOp = 13;
		*BranchOrJump = false;
		*Branch = true;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = true;
		break;

	case 0xe://1110 Bne
		*RegWrite = false;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 0;
		*ALUOp = 14;
		*BranchOrJump = false;
		*Branch = true;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = true;
		break;

	case 0xf://1111 Stli
		*RegWrite = true;
		*RegSrc = true;
		*RegDest = true;
		*ALUSrc = 1;
		*ALUOp = 15;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = true;
		*isBranch = false;
		break;

	default:
		*RegWrite = false;
		*RegSrc = false;
		*RegDest = false;
		*ALUSrc = 0;
		*ALUOp = 0;
		*BranchOrJump = false;
		*Branch = false;
		*MemRead = false;
		*MemWrite = false;
		*MemtoReg = false;
		*isBranch = false;
		break;
	}
	*RsMuxControl = 0;

	/*
	//
	//Fowarding Unit
	//

	//R Type Instructions
	if (OpCode<6){
		cur_Rs = (rawOp >> 8) & 0xf;
		cur_Rt = (rawOp >> 4) & 0xf;
		cur_Rd = rawOp & 0xf;



	}
	//Immediates that use Rd as an operand
	else if (OpCode == 13 || OpCode == 14 || OpCode == 12){
		cur_Rs = (rawOp >> 8) & 0x3;
		cur_Rt = (rawOp >> 10) & 0x3;
		cur_Rd = -1;
	}
	//All other I Type Instructions
	else{
		cur_Rs = (rawOp >> 8) & 0x3;
		cur_Rt = -1;
		cur_Rd = (rawOp >> 10) & 0x3;
	}
		
	
		//RS forwarding 
		if (cur_Rs == mem_Rd && mem_Rd != -1){
			*RsMuxControl = 1;
		}
		else if (cur_Rs == wb_Rd && wb_Rd != -1){
			*RsMuxControl = 2;
		}
		
	//RT forwarding
		if (cur_Rt == mem_Rd && mem_Rd != -1){
		*ALUSrc = 2;
		}
		else if (cur_Rt == wb_Rd && wb_Rd != -1)
		*ALUSrc = 3;
	//(ALUSrc is set above if it is a 0 or 1 so no need for default statement)

	//these operations must stay in this order 
	wb_Rd = mem_Rd;
	mem_Rd = cur_Rd;

	*/
	
	
	
	
	

}//end update control




