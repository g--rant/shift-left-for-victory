/*
*	File: test_driver.cpp
*	Authors: All of team Shift Left for Victory
*
*	Description: This file is the primary test module for the project. Each "Testing Suite" (be it unit, sub system, or whole system testing) is implment in this file as a 
*		discrete function that will called from inside of main().  
*
*		Any testing suite not being used should have its function call in main commented out using a line comment '//'.
*	
*		Please use the printf function to provide output to the tester. Flushing the output buffer might be necessary. 
*
*/


/*
* Include Header Files
*/
#include "adder.h"
#include "alu.h"
#include "control.h"
#include "dataMemory.h"
#include "instructionMemory.h"
#include "logicalAnd.h"
#include "mux2_1.h"
#include "programCounter.h"
#include "registerFile.h"
#include "shiftLeft.h"
#include "signExtend.h"

#include <string>
#include <iostream>
#include<ctime>

using namespace std;

/*
*	TEST SUITES
*/

//	Testing Suite for adder module
void test_adder(){
	int16_t  i_in1 = 0;
	int16_t  i_in2 = 0;
	int16_t  i_out = 0;
	int16_t* pi_in1 = &i_in1;
	int16_t* pi_in2 = &i_in2;
	int16_t* pi_out = &i_out;

	Adder add(pi_in1, pi_in2, pi_out);
	*pi_in1 = 1;
	*pi_in2 = 2;
	add.Add();
	if (*pi_out == *pi_in1 + *pi_in2)
		printf("Adder: %i is correct.\n", *pi_out);
	else
		printf("Adder: %i is incorrect.\n", *pi_out);
	add.~Adder();
}

//	Testing Suite for alu module
void test_alu(){
	int16_t i_in1 = 0;
	int16_t i_in2 = 0;
	int16_t i_out = 0;
	int16_t i_control = 0;
	bool b_isZero = false;

	int16_t* pi_in1 = &i_in1;
	int16_t* pi_in2 = &i_in2;
	int16_t* pi_out = &i_out;
	int16_t* pi_control = &i_control;
	bool* pb_isZero = &b_isZero;

	*pi_control = 1;
	*pi_in1 = 0;
	*pi_in2 = 1;
	Alu logic(pi_in1, pi_in2, pi_out, pi_control, pb_isZero);
	for (*pi_control; *pi_control < 15; (*pi_control)++)
	{
		switch (*pi_control)
		{
		case 1:
		{
				  logic.Operate();
				  if (*pi_out == 1)
					  printf("Alu.adder: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.adder: %i is incorrect.\n", *pi_out);
				  break;
		}
		case 2:
		{
				  *pi_in1 = 2;
				  *pi_in2 = 1;
				  *pi_out = 0;
				  logic.Operate();
				  if (*pi_out == 1)
					  printf("Alu.sub: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.sub: %i is incorrect.\n", *pi_out);
				  break;
		}
		case 3:
		{
				  *pi_in1 = 0;
				  *pi_in2 = 1;
				  logic.Operate();
				  if (*pi_out == 1)
					  printf("Alu.slt: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.slt: %i is incorrect.\n", *pi_out);
				  break;
		}
		case 4:
		{
				  logic.Operate();
				  if (*pi_out == 1)
					  printf("Alu.or: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.or: %i is incorrect.\n", *pi_out);
				  break;
		}
		case 5:
		{
				  logic.Operate();
				  if (*pi_out == 1)
					  printf("Alu.and: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.and: %i is incorrect.\n", *pi_out);
				  break;
		}
		case 8:
		{
				  *pi_in1 = 1;
				  *pi_in2 = 2;
				  logic.Operate();
				  if (*pi_out == 4)
					  printf("Alu.sll: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.sll: %i is incorrect.\n", *pi_out);
				  *pi_in1 = 0;
				  *pi_in2 = 1;
				  break;
		}
		case 9:
		{
				  *pi_in1 = 3;
				  *pi_in2 = 1;
				  logic.Operate();
				  if (*pi_out == 1)
					  printf("Alu.srl: %i is correct.\n", *pi_out);
				  else
					  printf("Alu.srl: %i is incorrect.\n", *pi_out);
				  *pi_in1 = 0;
				  *pi_in2 = 1;
				  break;
		}
		case 10:
		{
				   logic.Operate();
				   if (*pi_out == 1)
					   printf("Alu.addi: %i is correct.\n", *pi_out);
				   else
					   printf("Alu.addi: %i is incorrect.\n", *pi_out);
				   break;
		}
		case 11:
		{
				   
				   *pi_in1 = 2;
				   *pi_in2 = 0;
				   				   
				   logic.Operate();
				   if (*pi_out == *pi_in2 + *pi_in1)
					   printf("Alu.lw: %i is correct.\n", *pi_out);
				   else
					   printf("Alu.lw: %i is incorrect.\n", *pi_out);

				   break;
				   
		}
		case 12:
		{
			*pi_in1 = 1;
			*pi_in2 = 2;
		    logic.Operate();
			if (*pi_out == *pi_in1 + *pi_in2)
				printf("Alu.sw: %i is correct.\n", *pi_out);
			else
				printf("Alu.sw: %i is incorrect.\n", *pi_out);
			
			break;
		}
		case 13:
		{
			*pi_in2 = 1;
			*pi_in1 = 0;
			logic.Operate();
			if (*pi_out == 0)
				printf("Alu.beq: %i is correct.\n", *pi_out);
			else
				printf("Alu.beq: %i is incorrect.\n", *pi_out);
			break;
		}
		case 14:
		{
			logic.Operate();
			if (*pi_out == 1)
				printf("Alu.bne: %i is correct.\n", *pi_out);
			else
				printf("Alu.bne: %i is incorrect.\n", *pi_out);
			break;
		}
		case 15:
		{
			logic.Operate();
			if (*pi_out == 1)
				printf("Alu.slti: %i is correct.\n", *pi_out);
			else
				printf("Alu.slti: %i is incorrect.\n", *pi_out);
			break;
		}
		default:
		{
			logic.Operate();
			if (*pi_out == 0)
				printf("Alu.nop: %i is correct.\n", *pi_out);
			else
				printf("Alu.nop: %i is incorrect.\n", *pi_out);
			break;
		}

		}
	}
}

//	Testing Suite for the dataMemory module
void test_dataMemory(){

	bool b_writeTrue = false;
	bool b_readTrue = false;
	int16_t i_adder = 0;
	int16_t i_in = 0;
	int16_t i_out = 0;

	bool* pb_writeTrue = &b_writeTrue;
	bool* pb_readTrue = &b_readTrue;
	int16_t* pi_addr = &i_adder;
	int16_t* pi_in = &i_in;
	int16_t* pi_out = &i_out;


	
	*pb_readTrue = false;
	*pb_writeTrue = false;
	*pi_addr = 0;
	*pi_in = 0;
	*pi_out = 0;

	dataMemory Mem1(pb_writeTrue, pb_readTrue, pi_addr, pi_in, pi_out);
	*pb_readTrue = false;
	*pb_writeTrue = true;
	*pi_addr = 0;
	*pi_in = 45;
	Mem1.Write();
	if (Mem1.data_mem[*pi_addr] == *pi_in){
		printf("Write() works correctly. In: %i  Out: %i\n", *pi_in, Mem1.data_mem[*pi_addr]);
	}
	else{
		printf("Write() doesn't work. In: %i  Out: %i\n", *pi_in, Mem1.data_mem[*pi_addr]);
	}
	
	*pb_readTrue = true;
	*pb_writeTrue = false;
	Mem1.Read();

	if (*pi_out == *pi_in)
		printf("dataMemory.Read: %i is correct.\n", *pi_out);
	else
		printf("dataMemory.Read: %i is incorrect.\n", *pi_out);
	Mem1.~dataMemory();
	return;
	
}

//	Testing Suite for instructionMemory module
void test_instructionMemory(){
	int16_t i_addr = 0;
	int16_t i_output = 0;
	int16_t* pi_addr = &i_addr; 
	int16_t* pi_output = &i_output;
	InstructionMemory IMem(pi_addr, pi_output);
	
	for (int i = 0; i < 55; i++){
		*pi_addr = i*4;
		IMem.Read();
		printf("Instruction %i: %x\n", i, (int)*pi_output);
	}
	system("pause");
	}

//	Testing Suite for logicalAnd module
void test_logicalAnd(){
	bool b_in1, b_in2, b_out;

	bool *pb_in1 = &b_in1;
	bool *pb_in2 = &b_in2;
	bool *pb_out = &b_out;
	*pb_in1 = false;
	*pb_in2 = false;
	*pb_out = false;

	LogicalAnd and1(pb_in1, pb_in2, pb_out);
	*pb_in1 = true;
	*pb_in2 = false;
	and1.LogicalAndUpdate();
	if (*pb_out==false)
		printf("Logical And: %d is correct.\n", (int)*pb_out);
	else
		printf("Logical And: %d is incorrect.\n", (int)*pb_out);

	and1.~LogicalAnd();
	return;

}

//	Testing Suite for mux2_1 module
void test_mux2_1(){
	int16_t i_input0 = 0, i_input1 = 0, i_output = 0;
	bool b_control = false;
	int16_t* pi_input0 = &i_input0;
	int16_t* pi_input1 = &i_input1; 
	int16_t* pi_output = &i_output;
	bool* pi_control = &b_control;
	Mux2_1 mux(pi_input0, pi_input1, pi_output, pi_control);
	*pi_control = 0;
	mux.Select();
	if (*pi_output == *pi_input0)
		printf("Mux2_1: %i is correct.\n", *pi_control);
	else
		printf("Mux2_1: %i is incorrect.\n", *pi_control);

	*pi_control = 1;
	mux.Select();
	if (*pi_output == *pi_input1)
		printf("Mux2_1: %i is correct.\n", *pi_control);
	else
		printf("Mux2_1: %i is incorrect.\n", *pi_control);
	mux.~Mux2_1();
	return;
}

//	Testing Suite for programCounter module
void test_programCounter(){
	int16_t i_input = 0, i_output = 0;
	
	int16_t* pi_input = &i_input; 
	int16_t* pi_output = &i_output;

	bool b_blank = false;
	bool* pb_blank = &b_blank;
	
	ProgramCounter PC(pi_input, pi_output, pb_blank);
	*pi_input = 897; //arbitrary testing value
	PC.SetBranch();
	PC.GetAddr();
	if (*pi_output == *pi_input)
		printf("ProgramCounter: %i is correct.\n", *pi_output);
	else 
		printf("ProgramCounter: %i is incorrect.\n", *pi_output);
	PC.~ProgramCounter();
	return;

}

//	Testing Suite for the registerFile module
void test_registerFile(){
	int16_t i_RsAddr = 0;
	int16_t i_RtAddr = 0;
	int16_t i_RsData = 0;
	int16_t i_RtData = 0;
	int16_t i_RdAddr = 0;
	int16_t i_RdData = 0;
	bool b_writeFlag = false;
	//Create Data Lines
	int16_t* pi_RsAddr = &i_RsAddr;
	int16_t* pi_RtAddr = &i_RtAddr;
	int16_t* pi_RsData = &i_RsData;
	int16_t* pi_RtData = &i_RtData;

	int16_t* pi_RdAddr = &i_RdAddr;
	int16_t* pi_RdData = &i_RdData;

	bool* pb_writeFlag = &b_writeFlag;

	//Instantiate the RegisterFile Object
	RegisterFile regFile(pi_RsAddr, pi_RtAddr, pi_RsData, pi_RtData, pi_RdAddr, pi_RdData, pb_writeFlag);

	//Read the Registers and Output if the result is correct
	for (int i = 0; i < 16; i++){
		*pi_RsAddr = i;
		regFile.Read();
		printf("R%i holds the value %i.\n", i, *pi_RsData);
	}

	*pi_RdAddr = 15;
	*pi_RdData = 0xABCD;
	regFile.Write();

	*pi_RtAddr = 15;
	regFile.Read();
	printf("R15 holds the value %i.\n", *pi_RsData);

	regFile.~RegisterFile();
	return;
}

//	Testing Suite for shiftLeft1 module
void test_shiftLeft2(){
	int16_t i_in1= 0, i_out = 0;

	int16_t* pi_in1 = &i_in1; 
	int16_t* pi_out = &i_out;
	ShiftLeft SL(pi_in1, pi_out);
	*pi_in1 = 0x1;
	
	SL.Shift();
	if (*pi_out == 0x4)
		printf("shiftleft2: %i is correct.\n", *pi_out);
	else
		printf("shiftleft2: %i is incorrect.\n", *pi_out);
	SL.~ShiftLeft();
	return;
}

/*
//Testing Suite for the signExtend Module
void test_signExtend()
{
	int8_t val1 = 1;
	int16_t val2;
	int8_t* ptr_val = &val1;
	int16_t* ptr_val2 = &val2;
	signExtend e(ptr_val, ptr_val2);
	e.extend();
	printf("signExtend: %i \n", *ptr_val2);
}
*/



/*
*	MAIN
*	SHOULD ONLY CALL A SINGLE TEST SUITE AT A TIME. 
*	All of the "Heavy Lifting" should be done by the test suites.
*/

/*int main(){
	
	//test_alu();
	//test_programCounter();
	//test_shiftLeft2();
	//test_mux2_1();
	//test_logicalAnd();
	//test_adder();
	//test_signExtend();
	//test_registerFile();
	//test_dataMemory();
	test_instructionMemory();
	


	system("pause");



	return 0;

}*/