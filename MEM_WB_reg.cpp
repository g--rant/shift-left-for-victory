/**
*	File: MEM_WB_reg.cpp
*	Authors: Shift Left for Victory
*
*	Description: cpp file to implement a class that simulates the MEM/WB inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*		WB Enable
*		WB Select
*		Rd
*		Alu Result
*		Mem Out
**/

#include "MEM_WB_reg.h"

//Constructor
MEM_WB_reg::MEM_WB_reg(bool* pb_WBEnableIn, bool* pb_WBSelectIn, int16_t*pi_RDIn, int16_t*pi_ALUResultIn, int16_t*pi_MemOutIn,
	bool* pb_WBEnableOut, bool* pb_WBSelectOut, int16_t*pi_RDOut, int16_t*pi_ALUResultOut, int16_t*pi_MemOutOut){

	pb_WBEnableInput = pb_WBEnableIn;
	pb_WBSelectInput = pb_WBSelectIn;
	pi_RDInput = pi_RDIn;
	pi_ALUResultInput = pi_ALUResultIn;
	pi_MemOutInput = pi_MemOutIn;

	pb_WBEnableOutput = pb_WBEnableOut;
	pb_WBSelectOutput = pb_WBSelectOut;
	pi_RDOutput = pi_RDOut;
	pi_ALUResultOutput = pi_ALUResultOut;
	pi_MemOutOutput = pi_MemOutOut;
		
}//end constructor

//Destructor
MEM_WB_reg::~MEM_WB_reg(){
}//end Destructor

void MEM_WB_reg::MEM_WBFunct(){
	*pb_WBEnableOutput = *pb_WBEnableInput;
	*pb_WBSelectOutput = *pb_WBSelectInput;
	*pi_RDOutput = *pi_RDInput;
	*pi_ALUResultOutput = *pi_ALUResultInput;
	*pi_MemOutOutput = *pi_MemOutInput;
}//MEM_WBFunct