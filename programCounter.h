/**
*	File: programCounter.h
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/
#include<stdint.h>

#ifndef PROGRAMCOUNTER_H
#define PROGRAMCOUNTER_H

class ProgramCounter
{
private:
	int16_t i_instructionAddr;
	int16_t* pi_dataIn;
	int16_t* pi_dataOut;
	bool* pb_PC_Pause;

public:
	ProgramCounter(int16_t* pi_input, int16_t* pi_output, bool* pb_pause);

	~ProgramCounter();

	void SetBranch();

	void GetAddr();
};

#endif 	