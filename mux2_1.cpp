/**
*	File: mux2_1.cpp
*	Authors: Shift Left for Victory
*
*	Description: This file defines and implement a class that simulates the behavior of a 2
*	to 1 mux. 
*
**/

#include "mux2_1.h"


//Constructor
Mux2_1::Mux2_1(int16_t* pi_input0, int16_t* pi_input1, int16_t* pi_output, bool* pb_controlLine){
	pi_inputLine0 = pi_input0;
	pi_inputLine1 = pi_input1;
	pi_outputLine = pi_output;
	pb_control = pb_controlLine;
}

//Destructor
Mux2_1::~Mux2_1(){
	
}

//Sets the output point to the value of the appropiate input line's pointer
//Might need to add some error catcing in case an value > 1 is passed in. 
void Mux2_1::Select(){
	if(*pb_control == true){
		*pi_outputLine = *pi_inputLine1;
	}
	else{
		*pi_outputLine = *pi_inputLine0;
	}
}
