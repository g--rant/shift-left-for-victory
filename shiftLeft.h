/**
*	File: shiftLeft1.h
*	Authors: Shift Left for Victory
*
*	Description: shift left header file
*
**/
#include<stdint.h>

#ifndef SHIFTLEFT_H
#define SHIFTLEFT_H


class ShiftLeft
{
	private:
		int16_t* pi_input1;
		int16_t* pi_output;
	
	public:
		//construct
		ShiftLeft(int16_t* pi_in1, int16_t* pi_out);
		
		//destructor
		~ShiftLeft();
		
		//shift input left 1 time
		void Shift();
		
};//end ShiftLeft
#endif