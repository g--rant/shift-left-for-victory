/**
*	File: alu.cpp
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/

#include "alu.h"

Alu::Alu(int16_t* pi_in1, int16_t* pi_in2, int16_t* pi_out, int16_t* pi_aluControl, bool* pb_zero){
	pi_dataIn1 = pi_in1;//set member data equal to data passed in so it's stored 
	pi_dataIn2 = pi_in2;
	pi_dataOut = pi_out;
	pi_control = pi_aluControl;
	pb_isZero = pb_zero;

}
Alu::~Alu(){//Deallocates pointers to member data for cleanliness

}

void Alu::Operate(){
		switch (*pi_control)//switch statement based on control input
		{
		case 0x0001://add $d, $s, $t; $d = $s + $t
			*pi_dataOut = *pi_dataIn1 + *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x0002://sub $d, $s, $t; $d = $s $d
			*pi_dataOut = *pi_dataIn1 - *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x0003://slt $d, $s, $t; $d =($s < $t) ? 1 : 0
			if (*pi_dataIn1 < *pi_dataIn2){
				*pi_dataOut = 1;
			}
			else if (*pi_dataIn1 >= *pi_dataIn2)
			{
				*pi_dataOut = 0;
			}

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;
		case 0x0004://or $d = $s | $t
			*pi_dataOut = *pi_dataIn1 | *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x0005://xor $d = $s ^ $t
			*pi_dataOut = *pi_dataIn1 ^ *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x0008://sll $d, $s, Const; $d = $s << Const
			*pi_dataOut = *pi_dataIn1 << *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x0009://srl $d, $s, Const; $1d = $s >> Const
			*pi_dataOut = *pi_dataIn1 >> *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x000A://addi $d, $s, const; $d = $s + const
			*pi_dataOut = *pi_dataIn1 + *pi_dataIn2;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x000B://lw $d, Offset($s); $d = MEM16($s + Offset)
			*pi_dataOut = (*pi_dataIn1 + *pi_dataIn2); //Base addr + Offset
			

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x000C://sw $d, Offset($s); MEM16($s + Offset) = $d
			*pi_dataOut = (*pi_dataIn1 + *pi_dataIn2); //Base addr + Offset


			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		case 0x000D://beq $s, $d, Offset; If $s = $d, PC += Offset
			if (*pi_dataIn1 == *pi_dataIn2)
			{
				*pi_dataOut = 1;
				*pb_isZero = true;
			}
			else
			{
				*pi_dataOut = 0;
				*pb_isZero = false;
			}

			break;

		case 0x000E://bne $s, $d, Offset; If $s ≠ $d, PC += Offset
			if (*pi_dataIn1 != *pi_dataIn2)
			{
				*pi_dataOut = 1;
				*pb_isZero = true;
			}
			else
			{
				*pi_dataOut = 0;
				*pb_isZero = false;
			}


			break;

		case 0x000F://stli $d, $s, Const6; $d=($s<Const6) ? 1 : 0
			if (*pi_dataIn1 < *pi_dataIn2)
			{
				*pi_dataOut = 1;
			}
			else
			{
				*pi_dataOut = 0;
			}

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;

		default: //nop: sll $0, $0, 0
			*pi_dataOut = *pi_dataIn1 << 0;

			if (*pi_dataOut){
				*pb_isZero = false;
			}
			else{
				*pb_isZero = true;
			}
			break;
		}
		
		
		
	}

