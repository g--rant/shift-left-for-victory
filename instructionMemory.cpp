/**
*	File: instructionMemory.cpp
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/
//Headers
#include "instructionMemory.h"

//C++ streams for file parsing.
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void GetNoFoward(int16_t* ai_code){
	int16_t ai_noForwarding[] = { 
		0x3031,					//slt $1, $zero, $3			//0
		0x0000, 0x0000,										// 1, 2
		0xd439,					//beq $1, $zero, end loop	//3
		0x0000, 0x0000,										//4, 5	
		0xafff,					//addi $3, $3, 0d -1		//6
		0xb900,					//lw $1, 0($2)				//7
		0x0000, 0x0000,										// 8 9
		0x1018,					//add $8, $zero, $1			//10 
		0x0000, 0x0000,
		0x3a81,					//slt $1, $10, $8			//13
		0x0000,	0x0000, 
		0xd412,					//beq $1, $zero, else		//16
		0x0000, 0x0000, 
		0x1041,					//add $1, $zero, $4		//if logic   //19
		0x0000, 0x0000,
		0x9503,					//srl $1, $1, 0d3			//22
		0x0000, 0x0000, 
		0x1104,					//add $4, $1, $zero			//25
		0x0000, 0x0000, 
		0x4545,					//or $5, $5, $4				//28
		0x10c1,					//addi $1, $zero, 0xFF		//29
		0x0000, 0x0000, 
		0xc900,					//sw $1, 0($2)				//35
		0xaa02,					//addi $2, $2, 0d2			//36
		0x6000,					//j loop					//37
		0x1601,					//add $1, $6, $zero		//else logic	//38
		0x0000, 0x0000, 
		0x8501,					//sll $1, $1, 0d2      41
		0x0000, 0x0000, 
		0x1106,					//add $6, $1, $zero    44
		0x0000, 0x0000, 
		0x5767,					//xor $7, $7, $6   47
		0x10b1,					//addi $1, $zero, 0xFF    48
		0x0000, 0x0000,
		0xc900,					//sw $1, 0($2)  51
		0xaa02,					//addi $2, $2, 0d2    52
		0x6000,					//j loop    53
		0x0000, 0x0000, 0x0000, 0x0000, 0x0000}; //Terminator   54 55 56 57 58

	for (int i = 0; i < 59; i++){
		ai_code[i] = ai_noForwarding[i];
	}
	return;
}
void GetForward(int16_t* ai_code){
	int16_t ai_Forwarding[] = {
		0x3031,
		0xD41e,
		0x0000, 0x0000,
		0xAFFF,
		0xB900,
		0x0000,
		0x1018,
		0x3A81,
		0xD40C,
		0x0000, 0x0000,
		0x1041,
		0x9503,
		0x1104,
		0x4545,
		0x10C1,
		0xC900,
		0xAA02,
		0x6000,
		0x0000, 0x0000,
		0x1601,
		0x8501,
		0x1106,
		0x5767,
		0x10B1,
		0xC600,
		0xAA02,
		0x6000,
		0x0000, 0x0000, 0x0000, 0x0000, 0x0000

	};
	for (int i = 0; i < 36; i++)
		ai_code[i] = ai_Forwarding[i];
	return;
}

void TestCode(int16_t* ai_code){
	int16_t ai_noForwarding[] = {
		0x10c1,	//0		//add $1, $zero, %15
		0xc900,	//1
		0x0000,	//2
		0x0000, //3		//sw $1, 0($2)
		0x0000, //4
		0x0000, //5 
		0x0000, //6
		0x0000, //7
		0x0000, //8
		0x0000, //9
		0x0000, //a
		0x0000, //b
		0x0000, //c
		0x0000, //d
		0x0000, //e
		0x0000, //f
		0x0000, //10
		0x0000, 0x0000,
		0x0000,
		0x0000
	};
	for (int i = 0; i < 9; i++){
		ai_code[i] = ai_noForwarding[i];
	}
	return;

}



//Class Constructor
InstructionMemory::InstructionMemory(int16_t* pi_addr, int16_t* pi_output){
	pi_instructAddr = pi_addr;
	pi_instructCode = pi_output;
	ai_machineCode = (int16_t*)malloc(200); //No idea why 109 works, but 108 doesn't. Discuss with Philip and Chris
	GetNoFoward(ai_machineCode);
	//GetForward(ai_machineCode);
	//TestCode(ai_machineCode);
}

//Destructor Memory
InstructionMemory::~InstructionMemory(){
	
}

//Read the current instruction
void InstructionMemory::Read(){
	int16_t i_realAddr;
	if (*pi_instructAddr != 0){
		i_realAddr = (*pi_instructAddr) / 2;
	}
	else {
		i_realAddr = 0;
	}
	*pi_instructCode = ai_machineCode[i_realAddr];
}

