/**
*	File: mux4_1.cpp
*	Authors: Shift Left for Victory
*
*	Description: This file defines and implement a class that simulates the behavior of a 2
*	to 1 mux.
*
**/

#include "mux4_1.h"


//Constructor
Mux4_1::Mux4_1(int16_t* pi_input0, int16_t* pi_input1, int16_t* pi_input2, int16_t* pi_input3, int16_t* pi_output, int16_t* pi_controlLine){
	pi_inputLine0 = pi_input0;
	pi_inputLine1 = pi_input1;
	pi_inputLine2 = pi_input2;
	pi_inputLine3 = pi_input3;
	pi_outputLine = pi_output;
	pi_control = pi_controlLine;
}
Mux4_1::Mux4_1(int16_t* pi_input0, int16_t* pi_input1, int16_t* pi_input2, int16_t* pi_output, int16_t* pi_controlLine){
	pi_inputLine0 = pi_input0;
	pi_inputLine1 = pi_input1;
	pi_inputLine2 = pi_input2;
	pi_outputLine = pi_output;
	pi_control = pi_controlLine;
}

//Destructor
Mux4_1::~Mux4_1(){

}

//Sets the output point to the value of the appropiate input line's pointer
//Might need to add some error catcing in case an value > 1 is passed in. 
void Mux4_1::Select(){
	if (*pi_control == 0){
		*pi_outputLine = *pi_inputLine0;
	}
	else if(*pi_control == 1)
	{
		*pi_outputLine = *pi_inputLine1;
	}
	else if (*pi_control == 2)
	{
		*pi_outputLine = *pi_inputLine2;
	}
	else if (*pi_control == 3)
	{
		*pi_outputLine = *pi_inputLine3;
	}
	else
		*pi_outputLine = *pi_inputLine0;
}
