/**
*	File: IF_ID_reg.h
*	Authors: Shift Left for Victory
*
*	Description: h file to implement a class that simulates the IF_ID inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*	Lines in are:
*		Next Addr
*		Instruction
*	Lines out are:
*		NextAddr
*		ToControl
*		ToMux0Reg1
*		ToMux1Reg1
*		ToReadReg2
*		ToSignExtConst
*		ToRdMux0
*		ToRDMux1
**/
#include<stdint.h>
#ifndef IF_ID_REG_H
#define IF_ID_REG_H

class IF_ID_reg
{
private:
	int16_t* pi_NextAddrInput;
	int16_t* pi_InstructionInput;

	int16_t* pi_NextAddrOutput;
	int16_t* pi_ToControlOutput;
	int16_t* pi_ToMux0Reg1Output;
	int16_t* pi_ToMux1Reg1Output;
	int16_t* pi_ToReadReg2Output;
	int8_t* pi_ToSignExtConstOutput;
	int16_t* pi_ToRdMux0Output;
	int16_t* pi_ToRdMux1Output;
	bool* pb_Control_Pause;
public:
	//construct
	IF_ID_reg(int16_t* pi_NextAddrIn, 
		int16_t* pi_InstructionIn,
		int16_t* pi_NextAddrOut, 
		int16_t* pi_ToControlOut, 
		int16_t* pi_ToMux0Reg1Out, 
		int16_t* pi_ToMux1Reg1Out, 
		int16_t* pi_ToReadReg2Out, 
		int8_t* pi_ToSignExtConstOut, 
		int16_t* pi_ToRdMux0Out, 
		int16_t* pi_ToRdMux1Out,
		bool* pb_pause);


	//Destructor
	~IF_ID_reg();

	//Pass Over Function
	void IF_IDFunct();

};//end IF_ID_reg

#endif