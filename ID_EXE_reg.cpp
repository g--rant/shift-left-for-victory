/**
*	File: ID_EXE_reg.cpp
*	Authors: Shift Left for Victory
*
*	Description: cpp file to implement a class that simulates the ID/EXE inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*		WB Enable
*		WB Select
*		Mem Write Enable
*		Mem Read Enable
*		Mem doBranch
*		Exe Const/Rt Select
*		Exe Rd Addr Select
*		Exe ALU Op
*		Rs Value
*		Rt Value
*		Sign Ex Const
*		Rd Opt 1
*		Rd Opt 2
**/

#include "ID_EXE_reg.h"

//Constructor
ID_EXE_reg::ID_EXE_reg(int16_t* pi_NextAddrIn, bool* pb_WBEnableIn, bool* pb_WBSelectIn, bool* pb_MemWriteEnableIn, bool* pb_MemReadEnableIn, bool* pb_MemdoBranchIn, int16_t* pi_ExeConstRtSelectIn, bool* pb_ExeRdAddrSelectIn, int16_t* pi_ExeALUOpIn, int16_t* pi_RsValueIn, int16_t* pi_RtValueIn, int16_t* pi_SignExConstIn, int16_t* pi_RdOpt1In, int16_t* pi_RdOpt2In, int16_t* pi_RsMuxIn,
	int16_t* pi_NextAddrOut, bool* pb_WBEnableOut, bool* pb_WBSelectOut, bool* pb_MemWriteEnableOut, bool* pb_MemReadEnableOut, bool* pb_MemdoBranchOut, int16_t* pi_ExeConstRtSelectOut, bool* pb_ExeRdAddrSelectOut, int16_t* pi_ExeALUOpOut, int16_t* pi_RsValueOut, int16_t* pi_RtValueOut, int16_t* pi_SignExConstOut, int16_t* pi_RdOpt1Out, int16_t* pi_RdOpt2Out, int16_t* pi_RsMuxOut,
	bool* pb_pause){


	pi_NextAddrInput = pi_NextAddrIn;
	pb_WBEnableInput = pb_WBEnableIn;
	pb_WBSelectInput = pb_WBSelectIn;
	pb_MemWriteEnableInput = pb_MemWriteEnableIn;
	pb_MemReadEnableInput = pb_MemReadEnableIn;
	pb_MemdoBranchInput = pb_MemdoBranchIn;
	pi_ExeConstRtSelectInput = pi_ExeConstRtSelectIn;
	pb_ExeRdAddrSelectInput = pb_ExeRdAddrSelectIn;
	pi_ExeALUOpInput = pi_ExeALUOpIn;
	pi_RsValueInput = pi_RsValueIn;
	pi_RtValueInput = pi_RtValueIn;
	pi_SignExConstInput = pi_SignExConstIn;
	pi_RdOpt1Input = pi_RdOpt1In;
	pi_RdOpt2Input = pi_RdOpt2In;
	pi_RsMuxSelectInput = pi_RsMuxIn;
	
	pi_NextAddrOutput = pi_NextAddrOut;
	pb_WBEnableOutput = pb_WBEnableOut;
	pb_WBSelectOutput = pb_WBSelectOut;
	pb_MemWriteEnableOutput = pb_MemWriteEnableOut;
	pb_MemReadEnableOutput = pb_MemReadEnableOut;
	pb_MemdoBranchOutput = pb_MemdoBranchOut;
	pi_ExeConstRtSelectOutput = pi_ExeConstRtSelectOut;
	pb_ExeRdAddrSelectOutput = pb_ExeRdAddrSelectOut;
	pi_ExeALUOpOutput = pi_ExeALUOpOut;
	pi_RsValueOutput = pi_RsValueOut;
	pi_RtValueOutput = pi_RtValueOut;
	pi_SignExConstOutput = pi_SignExConstOut;
	pi_RdOpt1Output = pi_RdOpt1Out;
	pi_RdOpt2Output = pi_RdOpt2Out;
	pi_RsMuxSelectOutput = pi_RsMuxOut;

	pb_Control_Pause = pb_pause;
	
}//end construct

//Destructor
ID_EXE_reg::~ID_EXE_reg(){
}//end Destructor


//Function that shifts internal stage one (output from the Decode Stage) to internal stage two (output to the Execute Stage)
void ID_EXE_reg::ID_EXEFunct(){
	
	if (*pb_Control_Pause){
		*pi_NextAddrOutput = 0;
		*pb_WBEnableOutput = false;
		*pb_WBSelectOutput = false;
		*pb_MemWriteEnableOutput = false;
		*pb_MemReadEnableOutput = false;
		*pb_MemdoBranchOutput = false;
		*pi_ExeConstRtSelectOutput = 0;
		*pb_ExeRdAddrSelectOutput = false;
		*pi_ExeALUOpOutput = 0;
		*pi_RsValueOutput = 0;
		*pi_RtValueOutput = 0;
		*pi_SignExConstOutput = 0;
		*pi_RdOpt1Output = 0;
		*pi_RdOpt2Output = 0;
		*pi_RsMuxSelectOutput = 0;
	}
	else{
		*pi_NextAddrOutput = *pi_NextAddrInput;
		*pb_WBEnableOutput = *pb_WBEnableInput;
		*pb_WBSelectOutput = *pb_WBSelectInput;
		*pb_MemWriteEnableOutput = *pb_MemWriteEnableInput;
		*pb_MemReadEnableOutput = *pb_MemReadEnableInput;
		*pb_MemdoBranchOutput = *pb_MemdoBranchInput;
		*pi_ExeConstRtSelectOutput = *pi_ExeConstRtSelectInput;
		*pb_ExeRdAddrSelectOutput = *pb_ExeRdAddrSelectInput;
		*pi_ExeALUOpOutput = *pi_ExeALUOpInput;
		*pi_RsValueOutput = *pi_RsValueInput;
		*pi_RtValueOutput = *pi_RtValueInput;
		*pi_SignExConstOutput = *pi_SignExConstInput;
		*pi_RdOpt1Output = *pi_RdOpt1Input;
		*pi_RdOpt2Output = *pi_RdOpt2Input;
		*pi_RsMuxSelectOutput = *pi_RsMuxSelectInput;
	}

}//ID_EXEFunct


