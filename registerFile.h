/**
*	File: registerFile.h
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/
#include <stdint.h>

#ifndef REGISTERFILE_H
#define REGISTERFILE_H

class RegisterFile
{
private:
	//Allocate the 15 registers non-zero registers	
	int16_t ai_registers[15];

	//Pointers to data buses
	int16_t* pi_readReg1;
	int16_t* pi_readReg2;
	int16_t* pi_readData1;
	int16_t* pi_readData2;
	int16_t* pi_writeReg;
	int16_t* pi_writeData;
	bool* pb_controlRegWrite;

public:
	//Constructor
	RegisterFile(int16_t* pi_readR1, int16_t* pi_readR2, int16_t* pi_readD1, int16_t* pi_readD2, int16_t* pi_writeReg, int16_t* pi_data, bool * controlWrite);

	//Destructor
	~RegisterFile();

	//Function that outputs the contents of pi_readReg1 to pi_readData1
	void Read();

	//Funtion that sets the value of pi_writeReg to pi_writeData
	void Write();

	//Display State of register file to the user
	void OutputFile();

	void Output();

};

#endif