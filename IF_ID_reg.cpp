/**
*	File: IF_ID_reg.cpp
*	Authors: Shift Left for Victory
*
*	Description: cpp file to implement a class that simulates the IF_ID inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*	Lines in are:
*		Next Addr
*		Instruction
*	Lines out are:
*		NextAddr
*		ToControl
*		ToMux0Reg1
*		ToMux1Reg1
*		ToReadReg2
*		ToSignExtConst
*		ToRdMux0
*		ToRDMux1
**/

#include "IF_ID_reg.h"

//Constructor
IF_ID_reg::IF_ID_reg(int16_t* pi_NextAddrIn,
	int16_t* pi_InstructionIn,
	int16_t* pi_NextAddrOut,
	int16_t* pi_ToControlOut,
	int16_t* pi_ToMux0Reg1Out,
	int16_t* pi_ToMux1Reg1Out,
	int16_t* pi_ToReadReg2Out,
	int8_t* pi_ToSignExtConstOut,
	int16_t* pi_ToRdMux0Out,
	int16_t* pi_ToRdMux1Out,
	bool* pb_pause
	){

	pi_NextAddrInput = pi_NextAddrIn;
	pi_InstructionInput = pi_InstructionIn;
	
	pi_NextAddrOutput = pi_NextAddrOut;
	pi_ToControlOutput = pi_ToControlOut;
	pi_ToMux0Reg1Output = pi_ToMux0Reg1Out;
	pi_ToMux1Reg1Output = pi_ToMux1Reg1Out;
	pi_ToReadReg2Output = pi_ToReadReg2Out;
	pi_ToSignExtConstOutput = pi_ToSignExtConstOut;
	pi_ToRdMux0Output= pi_ToRdMux0Out;
	pi_ToRdMux1Output= pi_ToRdMux1Out;

	pb_Control_Pause = pb_pause;
	

}//end constructor

//Destructor
IF_ID_reg::~IF_ID_reg(){
}//end Destructor

void IF_ID_reg::IF_IDFunct(){

	if ((*pb_Control_Pause) != true){
		*pi_NextAddrOutput = *pi_NextAddrInput;
		//this section divides the decoded instruction into components
		//[15:0]
		*pi_ToControlOutput = *pi_InstructionInput;
		//[11:8]
		*pi_ToMux0Reg1Output = (*pi_InstructionInput >> 8) & 0xf;
		//0,0,[11:10]
		*pi_ToMux1Reg1Output = (*pi_InstructionInput >> 10) & 0x3;
		//[7:4]
		*pi_ToReadReg2Output = (*pi_InstructionInput >> 4) & 0xf;
		//[7:0]
		*pi_ToSignExtConstOutput = *pi_InstructionInput & 0xff;
		//[3:0]
		*pi_ToRdMux0Output = *pi_InstructionInput & 0xf;
		//0,0,[9:8]
		*pi_ToRdMux1Output = (*pi_InstructionInput >> 8) & 0x3;
	}
}//IF_IDFunct