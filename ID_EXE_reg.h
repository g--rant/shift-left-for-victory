/**
*	File: ID_EXE_reg.h
*	Authors: Shift Left for Victory
*
*	Description: h file to implement a class that simulates the ID/EXE inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*		WB Enable
*		WB Select
*		Mem Write Enable
*		Mem Read Enable
*		Mem doBranch
*		Exe Const/Rt Select
*		Exe Rd Addr Select
*		Exe ALU Op
*		Rs Value
*		Rt Value
*		Sign Ex Const
*		Rd Opt 1
*		Rd Opt 2
**/
#include<stdint.h>
#ifndef ID_EXE_REG_H
#define ID_EXE_REG_H

class ID_EXE_reg
{
private:
	int16_t* pi_NextAddrInput;
	bool* pb_WBEnableInput;
	bool* pb_WBSelectInput;
	bool* pb_MemWriteEnableInput;
	bool* pb_MemReadEnableInput;
	bool* pb_MemdoBranchInput;
	int16_t* pi_ExeConstRtSelectInput;
	bool* pb_ExeRdAddrSelectInput;
	int16_t* pi_ExeALUOpInput;
	int16_t* pi_RsValueInput;
	int16_t* pi_RtValueInput;
	int16_t* pi_SignExConstInput;
	int16_t* pi_RdOpt1Input;
	int16_t* pi_RdOpt2Input;
	int16_t* pi_RsMuxSelectInput;

	int16_t* pi_NextAddrOutput;
	bool* pb_WBEnableOutput;
	bool* pb_WBSelectOutput;
	bool* pb_MemWriteEnableOutput;
	bool* pb_MemReadEnableOutput;
	bool* pb_MemdoBranchOutput;
	int16_t* pi_ExeConstRtSelectOutput;
	bool* pb_ExeRdAddrSelectOutput;
	int16_t* pi_ExeALUOpOutput;
	int16_t* pi_RsValueOutput;
	int16_t* pi_RtValueOutput;
	int16_t* pi_SignExConstOutput;
	int16_t* pi_RdOpt1Output;
	int16_t* pi_RdOpt2Output;
	int16_t* pi_RsMuxSelectOutput;

	bool* pb_Control_Pause;

public:
	//construct
	ID_EXE_reg(int16_t* pi_NextAddrIn, bool* pb_WBEnableIn, bool* pb_WBSelectIn, bool* pb_MemWriteEnableIn, bool* pb_MemReadEnableIn, bool* pb_MemdoBranchIn, int16_t* pi_ExeConstRtSelectIn, bool* pb_ExeRdAddrSelectIn, int16_t* pi_ExeALUOpIn, int16_t* pi_RsValueIn, int16_t* pi_RtValueIn, int16_t* pi_SignExConstIn, int16_t* pi_RdOpt1In, int16_t* pi_RdOpt2In, int16_t* pi_RsMuxIn,
		int16_t* pi_NextAddrOut, bool* pb_WBEnableOut, bool* pb_WBSelectOut, bool* pb_MemWriteEnableOut, bool* pb_MemReadEnableOut, bool* pb_MemdoBranchOut, int16_t* pi_ExeConstRtSelectOut, bool* pb_ExeRdAddrSelectOut, int16_t* pi_ExeALUOpOut, int16_t* pi_RsValueOut, int16_t* pi_RtValueOut, int16_t* pi_SignExConstOut, int16_t* pi_RdOpt1Out, int16_t* pi_RdOpt2Out, int16_t* pi_RsMuxOut,
		bool* pb_pause);

	//Destructor
	~ID_EXE_reg();

	//Pass Over Function
	void ID_EXEFunct();

};//end ID_EXE_reg

#endif