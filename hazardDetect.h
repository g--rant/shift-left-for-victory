/*
file: hazardDetect.h

*/

#include<stdint.h>
#ifndef HAZARDDETECT_H
#define HAZARDDETECT_H

class hazardDetect{
private: 
	int16_t* pi_instruction;
	bool* pb_memRead;
	bool* pb_PC_Pause;
	bool* pb_IF_ID_Pause;
	bool* pb_Control_Pause; 

	uint16_t u16_nops = 0;
	bool b_flag = false;

public: 
	hazardDetect(int16_t* pi_IfIdRegOutput, bool* pb_IdExRegOutput, bool* pb_PCounter, bool* pb_IfIdRegister, bool* pb_ControlMux);

	~hazardDetect();

	void Detect();
};



#endif




