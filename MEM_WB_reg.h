/**
*	File: MEM_WB_reg.h
*	Authors: Shift Left for Victory
*
*	Description: h file to implement a class that simulates the MEM/WB inter-stage register of team Shift Left for Victory's RISC processor.
*	registers transfered are:
*		WB Enable
*		WB Select
*		Rd
*		Alu Result
*		Mem Out
**/
#include<stdint.h>

#ifndef MEM_WB_REG_H
#define MEM_WB_REG_H

class MEM_WB_reg
{
private:
	bool* pb_WBEnableInput;
	bool* pb_WBSelectInput;
	int16_t*pi_RDInput;
	int16_t*pi_ALUResultInput;
	int16_t*pi_MemOutInput;

	bool* pb_WBEnableOutput;
	bool* pb_WBSelectOutput;
	int16_t*pi_RDOutput;
	int16_t*pi_ALUResultOutput;
	int16_t*pi_MemOutOutput;

public:
	//construct
	MEM_WB_reg(bool* pb_WBEnableIn, bool* pb_WBSelectIn, int16_t*pi_RDIn, int16_t*pi_ALUResultIn, int16_t*pi_MemOutIn,
		bool* pb_WBEnableOut, bool* pb_WBSelectOut, int16_t*pi_RDOut, int16_t*pi_ALUResultOut, int16_t*pi_MemOut);


	//Destructor
	~MEM_WB_reg();

	//Pass Over Function
	void MEM_WBFunct();

};//end MEM_WB_reg

#endif