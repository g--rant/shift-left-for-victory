/**
*	File: mux4_1.h
*	Authors: Shift Left for Victory
*
*	Description:
*
**/
#include<stdint.h>

#ifndef MUX4_1_H
#define MUX4_1_H

class Mux4_1
{
private:
	int16_t * pi_inputLine0;
	int16_t * pi_inputLine1;
	int16_t * pi_inputLine2;
	int16_t * pi_inputLine3;

	int16_t * pi_outputLine;
	
	int16_t * pi_control;


public:
	Mux4_1(int16_t* pi_input0, int16_t* pi_input1, int16_t* pi_input2, int16_t *pi_input3, int16_t* pi_output, int16_t* pi_control);
	Mux4_1(int16_t* pi_input0, int16_t* pi_input1, int16_t* pi_input2, int16_t* pi_output, int16_t* pi_control);
	~Mux4_1();

	void Select();
};

#endif