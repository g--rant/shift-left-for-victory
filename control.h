/**
*	File: control.h
*	Authors: Shift Left for Victory
*
*	Description:This file implements the control structure of our hardware.
*
**/
#include<stdint.h>

#ifndef CONTROL_H
#define CONTROL_H
class Control
{
private:
	int16_t* ControlInput;
	bool* RegWrite;
	bool* RegSrc;
	bool* RegDest;
	int16_t* ALUSrc;
	int16_t* ALUOp;
	bool* Branch;
	bool* MemRead;
	bool* MemWrite;
	bool* MemtoReg;
	uint16_t OpCode;
	bool* BranchOrJump;
	bool* isBranch;
	int16_t *RsMuxControl;
	

	int16_t mem_Rd = 0;//prev 1 
	int16_t wb_Rd = 0;//prev 2
	int16_t cur_Rs = -1;
	int16_t cur_Rt = -1;
	int16_t cur_Rd = -1;
public:
	//Construct
	Control(int16_t* ControlInputIn, bool* RegWriteIn, bool* RegSrcIn,
		bool* RegDestIn, int16_t* ALUSrcIn, int16_t* ALUOpIn, bool* BranchIn, 
		bool* MemReadIn, bool* MemWriteIn, bool* MemtoRegIn, bool* BranchOrJumpIn, bool* isBranchIn, int16_t *isRsMuxControl);

	//Destructor
	~Control();

	//This does the work of the control function
	void UpdateControl();



};//end Control




#endif