/**
*	File: logicalAnd.h
*	Authors: Shift Left for Victory
*
*	Description: This is a header file for the implementation of logical and with 2 inputs and 1 output
*
**/
#ifndef LOGICALAND_H
#define LOGICALAND_H

class LogicalAnd
{
	private:
		bool* pb_input1;
		bool* pb_input2;
		bool* pb_output;
		
	public:
		//Construct
		LogicalAnd(bool* pb_in1, bool* pb_in2, bool* pb_out);
		
		//Destructor
		~LogicalAnd();
		
		//AndUpdate performs the and logical and
		void LogicalAndUpdate();

};//end And

#endif