/**
*	File: shiftLeft2.cpp
*	Authors: Shift Left for Victory
*
*	Description: cpp file for left shift
*
**/
#include "shiftLeft.h"
//construct
ShiftLeft::ShiftLeft(int16_t* pi_in1, int16_t* pi_out){
	//set pointers
	pi_input1 = pi_in1;
	pi_output = pi_out;	
}//end constructor

//destructor
ShiftLeft::~ShiftLeft(){

}//end destructor

//shift
void ShiftLeft::Shift(){
	*pi_output = *pi_input1 << 1;
}//end Shift