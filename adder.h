/**
*	File: adder.h
*	Authors: Shift Left for Victory
*
*	Description: basic adder header file
*
**/
#include<stdint.h>

#ifndef ADDER_H
#define ADDER_H

class Adder
{
	private:
		int16_t* pi_input1;
		int16_t* pi_input2;
		int16_t* pi_output;

	public:
		//construct
		Adder(int16_t* pi_in1, int16_t* pi_in2, int16_t* pi_out);
		
		//Destructor
		~Adder();
		
		//add 2 numbers
		void Add();
};//end ADDER

#endif