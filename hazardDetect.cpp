
#include "hazardDetect.h"
#include <string>


//constructor
hazardDetect::hazardDetect(int16_t* pi_IfIdRegOutput, bool* pb_IdExRegOutput, 
	bool* pb_PCounter, bool* pb_IfIdRegister, bool* pb_ControlMux){

	pi_instruction = pi_IfIdRegOutput;
	pb_memRead = pb_IdExRegOutput;
	pb_PC_Pause = pb_PCounter;
	pb_IF_ID_Pause = pb_IfIdRegister;
	pb_Control_Pause = pb_ControlMux;

}


//destructor
hazardDetect::~hazardDetect(){
}


//Operation function
void hazardDetect::Detect(){
	/*
	uint16_t OpCode;
	uint16_t rawOp = (uint16_t)(*pi_instruction);
	OpCode = rawOp >> 12;

	if (b_flag == false){

		if (OpCode == 9 || OpCode == 13 || OpCode == 14){
			u16_nops = 2;
			b_flag = true;
		}
		else if (*pb_memRead){
			u16_nops = 1;
			b_flag = true;
		}

		*pb_Control_Pause = false;
		*pb_IF_ID_Pause = false;
		*pb_Control_Pause = false;
	}
	else{
		if (u16_nops > 0){
			u16_nops--;
			*pb_Control_Pause = true;
			*pb_IF_ID_Pause = true;
			*pb_Control_Pause = true;
		}
		else
		{
			*pb_Control_Pause = false;
			*pb_IF_ID_Pause = false;
			*pb_Control_Pause = false;

			b_flag = false;
		}
	}
	*/
	*pb_Control_Pause = false;
	*pb_IF_ID_Pause = false;
	*pb_Control_Pause = false;


}