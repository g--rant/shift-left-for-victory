/**
*	File: adder.cpp
*	Authors: Shift Left for Victory
*
*	Description: cpp file for adder hardware
*
**/
#include "adder.h"


//constructor 
Adder::Adder(int16_t* pi_in1, int16_t* pi_in2, int16_t* pi_out){
	//set pointers
	pi_input1 = pi_in1;
	pi_input2 = pi_in2;
	pi_output = pi_out;

}//end constructor
	
	
//destructor
Adder::~Adder(){
}//end destructor

//adds 2 numbers and updates pi_result
void Adder::Add(){
	int16_t temp = *pi_input1 + *pi_input2;
	*pi_output = temp;
}//end Add