/**
*	File: dataMemory.cpp
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/

#include "dataMemory.h"
#include <stdio.h>
#include <iostream>
#include <fstream>



dataMemory::dataMemory(bool* pb_writeTrue, bool* pb_readTrue, int16_t* pi_addr, int16_t* pi_in, int16_t* pi_out)
{
	pb_writeData = pb_writeTrue;
	pb_readData = pb_readTrue;
	pi_memAddr = pi_addr;
	pi_dataIn = pi_in;
	pi_dataOut = pi_out;

	data_mem[0] = 0x0101;
	data_mem[1] = 0x0110;
	data_mem[2] = 0x0011;
	data_mem[3] = 0x00F0;
	data_mem[4] = 0x00FF;
	

}
dataMemory::~dataMemory()
{	//Deallocate member data
	
}


void dataMemory::Write()
{
	int index = (*pi_memAddr) - 16;
	index = index / 2;
	if(*pb_writeData != true)
		return;
	else if (*pi_memAddr > 63)
	{
		return;
		//data hazard -- throw hardware
		//if this happens, function will flip the fuck out and start flipping fucking tables and shit FUCK THIS SHIT	
	}
	else
		data_mem[index] = *pi_dataIn;
	return;
}


void dataMemory::Read()
{
	int index = (*pi_memAddr) - 16;
	index = index / 2;
	if(*pb_readData != true)//Check readData flag to make sure it is 1, otherwise break out of function
		return;
	else if(*pi_memAddr>63)
	{
		*pi_dataOut = 0;
	}
	else 
		*pi_dataOut = data_mem[index];
	return;

}

void dataMemory::OutputFile(){
	FILE * fo;
	fo = fopen("output.txt", "a");
	fprintf(fo, "The Memory Values are:\n");
	for (int x = 0; x < 15; x++){
		fprintf(fo, "$%i: %X\n", x, data_mem[x]);
	}
	fclose(fo);
}

void dataMemory::Output(){
	printf("The Memory Values are:\n");
	for (int x = 0; x < 5; x++){
		printf("mem[%i]: %X\n", x, data_mem[x]);
	}
}
