/**
*	File: instructionMemory.h
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/
#include<stdint.h>
#ifndef INSTRUCTIONMEMORY_H
#define INSTRUCTIONMEMORY_H



class InstructionMemory
{
private:
	int16_t* pi_instructAddr;
	int16_t* pi_instructCode;
	int16_t* ai_machineCode;

public:

	//Constructor
	InstructionMemory(int16_t* pi_addr, int16_t* pi_output);

	//Destructor
	~InstructionMemory();

	//Read the instruction currently indicated by pi_instructAddr and set pi_instruct Code
	//to the resulting value.
	void Read();
};

#endif