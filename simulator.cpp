/**
*	File: simulator.cpp
*	Authors: Shift Left for Victory
*
*	Description: This file defines the main() and implements the simulator for the Computer Architecture
*	(ECE 4713) Project.
*
**/

/*
* INCLUDES
*/
#include <fcntl.h>
#include <stdio.h>
#include<stdint.h>
#include <iostream>
#include <fstream>


#include "adder.h"
#include "alu.h"
#include "control.h"
#include "dataMemory.h"
#include "ID_EXE_reg.h"
#include "IF_ID_reg.h"
#include "EX_MEM_reg.h"
#include "MEM_WB_reg.h"
#include "instructionMemory.h"
#include "logicalAnd.h"
#include "mux2_1.h"
#include "mux4_1.h"
#include "programCounter.h"
#include "registerfile.h"
#include "shiftLeft.h"
#include "signExtend.h"

/*
* MACROS
*/ 
using namespace std;

void projectV1(){
	//
	//	Data Path Creation
	//
	
	//Fetch Stage
	
		int16_t i_nextAddr0 = 0;
		int16_t* pi_nextAddr0 = &i_nextAddr0;
		
		int16_t i_MuxOut = 0;
		int16_t* pi_MuxOut = &i_MuxOut;


		int16_t i_PCout = 0;
		int16_t* pi_PCout = &i_PCout;
		int16_t i_AdderConst2 = 2;
		int16_t* pi_AdderConst2 = &i_AdderConst2;
		int16_t i_InstructionMem = 0;
		int16_t* pi_InstructionMem = &i_InstructionMem;
	
	
	//Decode Stage
	
			//Next Address
		int16_t i_nextAddr1 = 0;
		int16_t* pi_nextAddr1 = &i_nextAddr1;

			//Control In
		int16_t i_control = 0;
		int16_t* pi_control = &i_control;
		

			//RegisterFile in (excluding Rd Addr and Rd Data)
		int16_t i_RsOpt0 = 0;
		int16_t* pi_RsOpt0 = &i_RsOpt0;
		//RsOpt0 and Rs Opt1 are both inputs to the mux between the register file and the IF/ID buffer
		int16_t i_RsOpt1 = 0;
		int16_t* pi_RsOpt1 = &i_RsOpt1;
		
		int16_t i_Rs = 0;	//i_Rs is the output of the mux going to the register file
		int16_t* pi_Rs = &i_Rs;
		
		int16_t i_Rt = 0;	//i_Rt is Read Register 2
		int16_t* pi_Rt = &i_Rt;
		
		bool b_RsSelect = false;	//Selects the correct Rs option from the mux -- is the control line
		bool* pb_RsSelect  = &b_RsSelect;

		/************************************************************
		RegFile Output variables
		*/
		int16_t i_Rs0 = 0;
		int16_t* pi_Rs0 = &i_Rs0;

		int16_t i_Rt0 = 0;
		int16_t* pi_Rt0 = &i_Rt0;

		int16_t i_Rt_Rd = 0;
		int16_t *pi_Rt_Rd = &i_Rt_Rd;

		/************************************************************/

			//Constant
		int8_t i_ConstPreExtend = 0;
		int8_t* pi_ConstPreExtend = &i_ConstPreExtend;

		int16_t i_Const0 = 0;
		int16_t* pi_Const0 = &i_Const0;

			//Rd Options
		int16_t i_Rd0_0 = 0;
		int16_t* pi_Rd0_0 = &i_Rd0_0;
		
		int16_t i_Rd1_0 = 0;
		int16_t* pi_Rd1_0 = &i_Rd1_0;

			//EXE Control Signals
		int16_t i_ALUOp0 = 0;
		int16_t* pi_ALUOp0 = &i_ALUOp0;
		int16_t i_RtConstSelect0 = false;
		int16_t* pi_RtConstSelect0 = &i_RtConstSelect0;
		bool b_RdAddrSelect0 = false;
		bool* pb_RdAddrSelect0 = &b_RdAddrSelect0;
		bool b_BranchOrJumpControl = false;
		bool* pb_BranchOrJumpControl = &b_BranchOrJumpControl;
		bool b_isBranch = false;
		bool* pb_isBranch = &b_isBranch;
		int16_t i_RsMuxControl0 = 0;
		int16_t* pi_RsMuxControl0 = &i_RsMuxControl0;
		int16_t i_RsMuxControl1 = 0;
		int16_t* pi_RsMuxControl1 = &i_RsMuxControl1;

			//MEM Control Signals
		bool b_doBranch0 = false;
		bool* pb_doBranch0 = &b_doBranch0;
		bool b_ReadEnable0 = false;
		bool* pb_ReadEnable0 = &b_ReadEnable0;
		bool b_WriteEnable0 = false;
		bool* pb_WriteEnable0 = &b_WriteEnable0;

			//WB Controls
		bool b_WBEnable0 = false;
		bool* pb_WBEnable0 = &b_WBEnable0;
		bool b_WBSelect0 = false;
		bool* pb_WBSelect0 = &b_WBSelect0;

			//Hazard Detection Unit Control 
		bool b_PC_pause = false;
		bool* pb_PC_pause = &b_PC_pause;
		bool b_IF_ID_Pause = false;
		bool* pb_IF_ID_Pause = &b_IF_ID_Pause;
		bool b_Control_Pause = false;
		bool* pb_Control_Pause = &b_Control_Pause;
	

	//Execute Stage
	
			//ALU
		int16_t i_Rs1 = 0;
		int16_t* pi_Rs1 = &i_Rs1;
		int16_t i_Rs2 = 0;
		int16_t* pi_Rs2 = &i_Rs2;
		int16_t i_RtConst = 0;
		int16_t* pi_RtConst = &i_RtConst;
		int16_t i_ALUOp1 = 0;
		int16_t* pi_ALUOp1 = &i_ALUOp1;
		int16_t i_ALU0 = 0;
		int16_t* pi_ALU0 = &i_ALU0;
		bool b_zero0 = false;
		bool* pb_zero0 = &b_zero0;

			//ALU Mux
		int16_t i_Rt1 = 0;
		int16_t* pi_Rt1 = &i_Rt1;
		int16_t i_Const1 = 0;
		int16_t* pi_Const1 = &i_Const1;
		int16_t i_RtConstSelect1 = 0;
		int16_t* pi_RtConstSelect1 = &i_RtConstSelect1;

			//Rd Selection Mux
		int16_t i_Rd0_1 = 0;
		int16_t* pi_Rd0_1 = &i_Rd0_1;
		int16_t i_Rd1_1 = 0;
		int16_t* pi_Rd1_1 = &i_Rd1_1;
		bool b_RdAddrSelect1 = false;
		bool* pb_RdAddrSelect1 = &b_RdAddrSelect1;
		int16_t i_RdFinal0 = 0;
		int16_t* pi_RdFinal0 = &i_RdFinal0;

			//Branch Address Calculation (const, shiftleft, adder)
		//const created in ALU Mux
		int16_t i_nextAddr2 = 0;
		int16_t* pi_nextAddr2 = &i_nextAddr2;
		int16_t i_shift = 0;
		int16_t* pi_shift = &i_shift;
		int16_t i_branchAddr0 = 0;
		int16_t* pi_branchAddr0 = &i_branchAddr0;
		int16_t i_branchAdder = 0;
		int16_t* pi_branchAdder = &i_branchAdder;
		int16_t i_branchAddr1 = 0;
		int16_t* pi_branchAddr1 = &i_branchAddr1;

			//MEM Control Signals
		bool b_doBranch1 = false;
		bool* pb_doBranch1 = &b_doBranch1;
		bool b_ReadEnable1 = false;
		bool* pb_ReadEnable1 = &b_ReadEnable1;
		bool b_WriteEnable1 = false;
		bool* pb_WriteEnable1 = &b_WriteEnable1;

			//WB Controls
		bool b_WBEnable1 = false;
		bool* pb_WBEnable1 = &b_WBEnable1;
		bool b_WBSelect1 = false;
		bool* pb_WBSelect1 = &b_WBSelect1;
	
		
	//Memory Stage 
	
			//Carry to next interStage reg
		int16_t i_RdFinal1 = 0;
		int16_t* pi_RdFinal1 = &i_RdFinal1;

			//Data Memory Block
		int16_t i_ALU1 = 0;
		int16_t* pi_ALU1 = &i_ALU1;
		int16_t i_Rt2 = 0;
		int16_t* pi_Rt2 = &i_Rt2;
		int16_t i_MemOut0 = 0;
		int16_t* pi_MemOut0 = &i_MemOut0;
		bool b_ReadEnable2 = false;
		bool* pb_ReadEnable2 = &b_ReadEnable2;
		bool b_WriteEnable2 = false;
		bool* pb_WriteEnable2 = &b_WriteEnable2;

			//And Gate
		bool b_zero1 = false;
		bool* pb_zero1 = &b_zero1;
		bool b_doBranch2 = false;
		bool* pb_doBranch2 = &b_doBranch2;
		bool b_branchSelect = false;
		bool* pb_branchSelect = &b_branchSelect;


			//WB Controls
		bool b_WBEnable2 = false;
		bool* pb_WBEnable2 = &b_WBEnable2;
		bool b_WBSelect2 = false;
		bool* pb_WBSelect2 = &b_WBSelect2;
	

	//Write Back Stage
	
			//Back the RegisterFile
		int16_t i_RdFinal2 = 0;
		int16_t* pi_RdFinal2 = &i_RdFinal2;
		bool b_WBEnable3 = false;
		bool* pb_WBEnable3 = &b_WBEnable3;

			//WB Selection Mux
		int16_t i_MemOut1 = 0;
		int16_t* pi_MemOut1 = &i_MemOut1;
		int16_t i_ALU2 = 0;
		int16_t* pi_ALU2 = &i_ALU2;
		bool b_WBSelect3 = false;
		bool* pb_WBSelect3 = &b_WBSelect3;

		/**************************************
		WB Selection Mux output		
		***************************************/
		int16_t i_return = 0;
		int16_t* pi_return = &i_return;

		/**************************************/

	
		//
		//Instatiate Blocks and Link the Data Path together.
		//

		//Fetch stage
		Mux2_1 PC_MUX(pi_nextAddr0, pi_branchAddr1, pi_MuxOut, pb_branchSelect);

		ProgramCounter PC(pi_MuxOut, pi_PCout, pb_PC_pause);

		Adder PC_Adder(pi_AdderConst2, pi_PCout, pi_nextAddr0);

		InstructionMemory InstructMem(pi_PCout, pi_InstructionMem);
		
		
		//Decode stage
		IF_ID_reg IfId_Buffer(pi_nextAddr0, pi_InstructionMem, pi_nextAddr1, pi_control, 
			pi_RsOpt0, pi_RsOpt1, pi_Rt, pi_ConstPreExtend, pi_Rd0_0, pi_Rd1_0, pb_IF_ID_Pause);
		
		Control Control_unit(pi_control, pb_WBEnable0, pb_RsSelect, pb_RdAddrSelect0, 
			pi_RtConstSelect0, pi_ALUOp0, pb_doBranch0, pb_ReadEnable0, pb_WriteEnable0, pb_WBSelect0, pb_BranchOrJumpControl, pb_isBranch, pi_RsMuxControl0);
		
		Mux2_1 decodeMux1(pi_RsOpt0, pi_RsOpt1, pi_Rs, pb_RsSelect);

		Mux2_1 decodeMux2(pi_Rt, pi_Rd1_0, pi_Rt_Rd, pb_isBranch);

		RegisterFile RegFile(pi_Rs, pi_Rt_Rd, pi_Rs0, pi_Rt0, pi_RdFinal2, pi_return, pb_WBEnable3);

		signExtend signExtendBlock(pi_ConstPreExtend, pi_Const0);
		
		//Execute stage
		ID_EXE_reg IdExe_Buffer(pi_nextAddr1, pb_WBEnable0, pb_WBSelect0, pb_WriteEnable0, 
			pb_ReadEnable0, pb_doBranch0, pi_RtConstSelect0, pb_RdAddrSelect0, pi_ALUOp0,
			pi_Rs0, pi_Rt0, pi_Const0, pi_Rd0_0, pi_Rd1_0, pi_RsMuxControl0,
								pi_nextAddr2, pb_WBEnable1, pb_WBSelect1, pb_WriteEnable1, 
			pb_ReadEnable1, pb_doBranch1, pi_RtConstSelect1, pb_RdAddrSelect1, pi_ALUOp1,
			pi_Rs1, pi_Rt1, pi_Const1, pi_Rd0_1, pi_Rd1_1, pi_RsMuxControl1, pb_Control_Pause);

		Mux2_1 Ex_RdMux(pi_Rd0_1, pi_Rd1_1, pi_RdFinal0, pb_RdAddrSelect1);

		Mux4_1 ExRtMux(pi_Rt1, pi_Const1, pi_ALU1, pi_return ,pi_RtConst, pi_RtConstSelect1);
		
		Mux4_1 ExRsMux(pi_Rs1, pi_ALU1, pi_return, pi_Rs2, pi_RsMuxControl1);

		ShiftLeft shift(pi_Const1, pi_shift);

		Adder Add(pi_nextAddr2, pi_shift, pi_branchAdder);

		Mux2_1 BranchOrJump(pi_branchAdder, pi_shift, pi_branchAddr0, pb_BranchOrJumpControl);

		Alu ALU(pi_Rs2, pi_RtConst, pi_ALU0, pi_ALUOp1, pb_zero0);

		//Memory stage
		EX_MEM_reg ExMem_Buffer(pb_WBEnable1, pb_WBSelect1, pb_WriteEnable1,
			pb_ReadEnable1, pb_doBranch1, pi_Rt1, pi_RdFinal0, pb_zero0, pi_ALU0, pi_branchAddr0,
			pb_WBEnable2, pb_WBSelect2, pb_WriteEnable2, pb_ReadEnable2, pb_doBranch2, 
			pi_Rt2, pi_RdFinal1, pb_zero1, pi_ALU1, pi_branchAddr1);

		LogicalAnd and(pb_doBranch2, pb_zero1, pb_branchSelect);
		
		dataMemory DMem(pb_WriteEnable2, pb_ReadEnable2, pi_ALU1, pi_Rt2, pi_MemOut0);

		//Writeback stage
		MEM_WB_reg MemWb_Buffer(pb_WBEnable2, pb_WBSelect2, pi_RdFinal1, pi_ALU1, 
			pi_MemOut0, pb_WBEnable3, pb_WBSelect3, pi_RdFinal2, pi_ALU2, pi_MemOut1);

		Mux2_1 wb_mux(pi_MemOut1, pi_ALU2, pi_return, pb_WBSelect3);
		
		/*
		*
		*	Script
		*
		*/
	//Set up
		bool b_EndOfProgram = false;
		int counter = 0;

		//ofstream myfile;
		//myfile.open("output.txt");

		while (!b_EndOfProgram){

			//**Write Back Stage
				//WBMux Select
				wb_mux.Select();
			
			//**Mem Stage
				//Data Memory Operate
				DMem.Read();
				DMem.Write();
				and.LogicalAndUpdate();

			//**Execute Stage
				//shift left Operater
				shift.Shift();
				//Add Operate
				Add.Add();
				//Rt/Const Mux select
				ExRtMux.Select();
				//Rd Mux select
				Ex_RdMux.Select();
				//Rs Mux Select
				ExRsMux.Select();
				//ALU Operate
				ALU.Operate();
				//Branch or Jump
				BranchOrJump.Select();

			//**Decode Stage
				//Control creates signals
				Control_unit.UpdateControl();
				//Sign-Extend Operate
				signExtendBlock.extend();
				//Rs Mux Select
				decodeMux1.Select();
				decodeMux2.Select();
				//Register File Operate
				RegFile.Write();
				RegFile.Read();
			

			//**Fetch Stage
				//PC Mux Select
				PC_MUX.Select();
				//PC Address Operate
				PC.SetBranch();
				PC.GetAddr();
				//PC Adder Add
				PC_Adder.Add();
				//Instruction Memory Operate
				InstructMem.Read();


			//**Update Inter-Stage Buffers
				MemWb_Buffer.MEM_WBFunct();
				ExMem_Buffer.EX_MEMFunct();
				IdExe_Buffer.ID_EXEFunct();
				IfId_Buffer.IF_IDFunct();
			
			
			
			
			/*FILE * fo;
			fo = fopen("output.txt", "a");
			fputs("Instruction: \n", fo);
			fclose(fo);
			RegFile.Output();//Just put this FILE stuff in your functions
			fo = fopen("output.txt", "a");
			fputs("\n", fo);
			fclose(fo);
			DMem.Output();
			fo = fopen("output.txt", "a");
			fputs("**********************************\n", fo);
			fclose(fo);*/
			
			if (*pi_MuxOut == 0x0000){

				printf("Loop %i: \n", counter);
				RegFile.Output();
				printf("\n");
				DMem.Output();
				printf("*************************************\n");

				counter++;
			}
			
			
			if (*pi_PCout == 0x0074){	//# of instructions *2			
				b_EndOfProgram = true;
			}
		}
		system("pause");
		
}



int main(int argc, const char* argv[])
{
	projectV1();
}

