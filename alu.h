/**
*	File: alu.h
*	Authors: Shift Left for Victory
*	Description: 
*
**/
#include<stdint.h>

#ifndef ALU_H
#define ALU_H

class Alu
{
private:
	int16_t* pi_dataIn1;
	int16_t* pi_dataIn2;
	int16_t* pi_dataOut;
	int16_t* pi_control;
	bool* pb_isZero;

	//Private functions used by Operate(). None needed.

public:
	//Constructor
	Alu(int16_t* pi_in1, int16_t* pi_in2, int16_t* pi_out, int16_t* pi_aluControl, bool* pb_zero);

	//Destructor
	~Alu();

	//This function performs the operation on the data coming in from pi_dataIn1, and pi_dataIn2
	//The operation it performs are base on the pi_control line's value. 
	void Operate();
	
		

//Nothing private, everything is visible to everyone	


};


#endif 	