/**
*	File: dataMemory.h
*	Authors: Shift Left for Victory
*	
*	Description: 
*
**/
#include<stdint.h>

#ifndef DATAMEMORY_H
#define DATAMEMORY_H

class dataMemory
{
private: 
	bool* pb_writeData;
	bool* pb_readData;
	int16_t* pi_memAddr;
	int16_t* pi_dataIn;
	int16_t* pi_dataOut;
	


public: 
	int16_t data_mem[64];

	dataMemory(bool* pb_writeTrue, bool* pb_readTrue, int16_t* pi_addr, int16_t* pi_in, int16_t* pi_out);
	
	~dataMemory();

	void Write();

	void Read();

	void OutputFile();

	void Output();

};

#endif
