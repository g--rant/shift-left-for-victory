/**
*	File: mux2_1.h
*	Authors: Shift Left for Victory
*
*	Description:  
*
**/
#include<stdint.h>

#ifndef MUX2_1_H
#define MUX2_1_H

class Mux2_1
{
private:
	int16_t * pi_inputLine0;
	int16_t * pi_inputLine1;
	int16_t * pi_outputLine;
	bool * pb_control;


public:
	Mux2_1(int16_t* input1, int16_t* input2, int16_t* output, bool* control);

	~Mux2_1();
	
	void Select();
};

#endif