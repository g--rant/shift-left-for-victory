/**
*	File: and.cpp
*	Authors: Shift Left for Victory
*
*	Description: This class contains the logic for a 2 input logical and
*
**/
#include "logicalAnd.h"

//Constructor
LogicalAnd::LogicalAnd(bool* pb_in1, bool* pb_in2, bool* pb_out ){
	//Set Pointers (data buses)
	pb_input1 = pb_in1;
	pb_input2 = pb_in2;
	pb_output = pb_out;
}//end Constructor

//Destructor
LogicalAnd::~LogicalAnd(){
	//delete pointer references
	
}//end destructor

//AndUpdate performs the and logical and
void LogicalAnd::LogicalAndUpdate(){
	*pb_output = *pb_input1 && *pb_input2;
}//end LogicalAndUpdate

