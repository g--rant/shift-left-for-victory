/**
*	File: registerFile.cpp
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/
#include "registerFile.h"
#include <stdio.h>



//Constructor
RegisterFile::RegisterFile(int16_t* pi_readR1, int16_t* pi_readR2, int16_t* pi_readD1, int16_t* pi_readD2, int16_t* pi_writeR, int16_t* pi_data, bool * pb_controlWrite){
	//Set Data Buses
	pi_readReg1 = pi_readR1;
	pi_readReg2 = pi_readR2;
	pi_readData1 = pi_readD1;
	pi_readData2 = pi_readD2;
	pi_writeReg = pi_writeR;
	pi_writeData = pi_data;
	pb_controlRegWrite = pb_controlWrite;

	//Initialize Registers
	//Register0 is a const 0 defined in the header.
	ai_registers[0] = 0x0000;		//r1	used for immediates
	ai_registers[1] = 0x0010;		//r2	$a0
	ai_registers[2] = 0x0005;		//r3	$a1
	ai_registers[3] = 0x0040;		//r4	$v0
	ai_registers[4] = 0x1010;		//r5	$v1
	ai_registers[5] = 0x000f;		//r6	$v2
	ai_registers[6] = 0x00f0;		//r7	$v3
	ai_registers[7] = 0x0000;		//r8	$t0
	ai_registers[8] = 0;			//r9	
	ai_registers[9] = 0x0100;		//r10	comparison value for the if
	ai_registers[10] = 0x00FF;		//r11	used in Else
	ai_registers[11] = 0xFF00;		//r12	used in If
	ai_registers[12] = 0;			//r13	not used
	ai_registers[13] = 0;			//r14	not used
	ai_registers[14] = 0x1234;			//r15	not used
}

//Destructor
RegisterFile::~RegisterFile(){
	//Delete all of the pointers
	
}

//Function assigns the values of the registers being read to the appropiate output pointer
void RegisterFile::Read(){
	//Local Variables
	int Rs = *pi_readReg1;
	int Rt = *pi_readReg2;

	//If Rs is the 0 register set the data out line to 0
	if(Rs == 0){
		*pi_readData1 = 0;
	}
	//Else set the data out line to the value of the register designate by Rs
	else{
		*pi_readData1 = ai_registers[Rs - 1];
	}
	
	//If Rt is the 0 register set the data out line to 0
	if(Rt == 0){
		*pi_readData2 = 0;
	}
	//Else set the data out line to the value of the register designate by Rt
	else{
		*pi_readData2 = ai_registers[Rt-1];
	}

} //End RegisterFile::Read()

void RegisterFile::Write(){
	//Local Variables
	int Rd = *pi_writeReg;

	//if Write signal is asserted, write to register
	if(*pb_controlRegWrite){
		//IF Rd is not the Zero Register
		if(Rd != 0){
			ai_registers[Rd-1] = *pi_writeData;
		}
		/*
		else{
			//throw hardwareError Exception
		}
		*/
	}
	//else do nothing

} //End RegisterFile::Write()

void RegisterFile::OutputFile(){
	FILE * fo;
	fo = fopen("output.txt", "a");
	fprintf(fo, "The register values are:\n");
	for (int x = 0; x < 15; x++){
		fprintf(fo, "$%i: %X  ", x, ai_registers[x]);
	}
	fclose(fo);
}

void RegisterFile::Output(){
	printf("r4: %X  r5: %X  r6: %X  r7: %X\n", ai_registers[3], ai_registers[4],
		ai_registers[5], ai_registers[6]);
	printf("r8: %X  r2 %X, r3 %X\n", ai_registers[7], ai_registers[1],
		ai_registers[2]);
}
