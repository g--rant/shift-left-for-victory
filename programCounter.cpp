/**
*	File: programCounter.cpp
*	Authors: Shift Left for Victory
*
*	Description: 
*
**/
#include "programCounter.h"

//Constructor
ProgramCounter::ProgramCounter(int16_t* pi_input, int16_t* pi_output, bool* pb_pause){
	pi_dataIn = pi_input;
	pi_dataOut = pi_output;
	i_instructionAddr = 0;
	pb_PC_Pause = pb_pause;
}

ProgramCounter::~ProgramCounter(){
	
}

void ProgramCounter::SetBranch(){ 

	if ((*pb_PC_Pause) == false){
		i_instructionAddr = *pi_dataIn;
	}
}

void ProgramCounter::GetAddr(){
	*pi_dataOut = i_instructionAddr;
}